# Unity GitLab CI/CD Example
## Getting started
1. Copy CI file to root of your project
2. Update the Unity version according to your project version in the CI file. All versions are available at [Gableroux DockerHub Page](https://hub.docker.com/r/gableroux/unity3d/)
3. Copy build script [AutomatedBuildProcess.cs](Assets/Editor/AutomatedBuildProcess.cs) (make sure you use the same path as original)
4. Follow how to activate instructions

## Activating Unity in Ubuntu 18.04 LTS

### Instaling Docker

> All commands with  `$` prefix should be executed inside terminal  

1. Open terminal `Ctrl + Alt + T`  
2. Download latest system updates `$ sudo apt update && sudo apt upgrade`  
3. Install docker `$ sudo apt install docker.io`  
4. Starting docker `$ sudo systemctl start docker`  
5. Verify installation `$ docker -v`  

> If you want docker to start automatically with system run `$ sudo systemctl enable docker`

### Pulling the Image
1. Check available images at [Gableroux DockerHub Page](https://hub.docker.com/r/gableroux/unity3d/)
2. Pull image `$ sudo docker pull gableroux/unity3d:2019.1.3f1`  
3. Define unity version `$ UNITY_VERSION=2019.1.3f1`  
4. Define other variables `$ FILE_NAME=Unity_v && FILE_EXTENSION=.alf`  
5. Run unity container bash  `$ docker run -it gableroux/unity3d:$UNITY_VERSION bash`  
6. Go to unity executable file directory `$ cd /opt/Unity/Editor`  
7. Create manual activation file `$ ./Unity -quit -batchmode -nographics -logFile /dev/stdout -createManualActivationFile`  
8. Show activation XML file contents `$ cat $FILE_NAME$UNITY_VERSION$FILE_EXTENSION`  
9. Create a file named `Unity_v*.alf` eg: `Unity_v2019.1.3f1.alf` with all the content from  `<?xml` to `</root>`  
10. Go to [Unity Manual Activation Page](https://license.unity3d.com/manual)  
> You should be logged in unity website during the creation procedure
11. Upload the `.alf` file to retrieve the `Unity_v*.x.ulf` file eg: `Unity_v2019.x.ulf`  
12. In `GitLab` go to your repository `Settings -> CI/CD -> Variables`  
13. Create new variable as describred:  

| Type       | Key                     | Value                                                    |
| :--------- | :---------------------- | :------------------------------------------------------- |
| `Variable` | `UNITY_LICENSE_CONTENT` | Copy & paste all the contents from `Unity_v*.x.ulf` file |