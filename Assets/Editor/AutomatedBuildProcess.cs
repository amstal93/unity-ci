﻿using System.IO;
using UnityEditor;
using UnityEngine;
using System.Linq;

/// <summary> Used by a batch script (-executeMethod) to automate building process for Unity from commandline or with continuous integration </summary>
public static class AutomatedBuildProcess
{
    private static string BUILD_FOLDER_PATH = "./Windows64/";
    private static string FILE_NAME = "Game.exe";
    
    public static void Build()
    {
        // Get list of scenes that are enabled in the Build Settings
        string[] enabledScenesPaths = EditorBuildSettings.scenes.Where(s => s.enabled).Select(s => s.path).ToArray();

        // Create a directory to put build and the data file
        if (!Directory.Exists(BUILD_FOLDER_PATH))
            Directory.CreateDirectory(BUILD_FOLDER_PATH);

        // Create build options and start the build
        Debug.Log("Starting to build for Windows x64 platform");
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = enabledScenesPaths,
            locationPathName = BUILD_FOLDER_PATH + FILE_NAME,
            target = BuildTarget.StandaloneWindows64,
            options = BuildOptions.None
        };
        BuildPipeline.BuildPlayer(buildPlayerOptions);
    }
}